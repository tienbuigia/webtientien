# clone same basic facebook apis

## TODO

- [ ] signup
- [ ] login
<!-- - [ ] logout -->
- [ ] add_post
- [ ] get_post
- [ ] edit_post
- [ ] delete_post
- [ ] report_post
- [ ] like
- [ ] get_comment
- [ ] set_comment
- [ ] get_list_posts
- [ ] check_new_item

## signup

- request: POST

### signup: _input_

- phonenumber: 10 numbers; start with 0.
- password: 6 - 10 symbols.
- uuid (haven't know how to yet)

### signup: _output_

- code
- message
- data (json)
- response:
  - successed: auth code? saved on server
  - failed: fail notice

## login

- request: POST
- data:

### login: _input_

- phonenumber
- password
- device id

### login: _output_

- code (response common)
- message
- userinfo:
  - id
  - username
  - token
  - avatar (-1: wait for change; 1: actived)
  - ~~active~~
- response:
  - successed:
    1. success notification
    2. redirect to homepage

## logout

- request: POST
- data:

### logout: _input_

- token

### logout: _output_

- code
- message

## add_post

### add_post: _input_

- token
- IMAGE/VIDEO
- DESCRIPTION
- STATUS

### add_post: _output_

- code
- message
- data:
  - id
  - url

## get_post

(capital letters mean optional)

- request: POST

### get_post: _input_

- TOKEN
- id

### get_post: _output_

- code
- message
- data:
  - id
  - description
  - created
  - modified
  - like
  - comment
  - is_liked
  - IMAGE
    - ID
    - URL
  - VIDEO
    - URL
    - THUMB
  - author
    - id
    - name
    - avatar
    - online
  - STATE
  - is_blocked
  - can_edit
  - banned
  - can_comment
  - url
  - messages

## edit_post

- request: POST

### edit_post: _input_

- token
- id
- DESCRIPTION
- STATUS
- IMAGE
- IMAGE_DEL
- IMAGE_SORT
- VIDEO
- THUMB
- AUTO_ACCEPT
- AUTO_BLOCK

### edit_post: _output_

- code
- message

## delete_post

- request: POST

### delete_post: _input_

- token
- id

### delete_post: _output_

- code
- message

## report_post

- request: POST

### report_post: _input_

- token
- id
- subject
- details

### report_post: _output_

- code
- message

## like

- request: POST

### like: _input_

- token
- id

### like: _output_

- code
- message
- data:
  - like

## get_comment

- request: POST

### get_comment: _input_

- id
- index
- count

### get_comment: _output_

- code
- message
- data:
  - id
  - comment
  - created
  - poster
    - id
    - name
    - avatar
- is_blocked

## set_comment

- request: POST

### set_comment: _input_

- token
- id
- comment
- index
- count

### set_comment: _output_

- code
- message
- data:
  - id
  - comment
  - created
  - poster
    - id
    - name
    - avatar
- is_blocked

## get_list_posts

- request: POST

### get_list_posts: _input_

- TOKEN
- USER_ID
- IN_CAMPAIGN
- CAMPAIGN_ID
- LATITUDE
- LONGITUDE
- LAST_ID
- index
- count

### get_list_posts: _output_

- code
- message
- data:
  - posts
    - id
    - name
    - IMAGE
    - VIDEO {url/thumb}
    - description
    - created
    - like
    - comment
    - is_liked
    - is_blocked
    - can_comment
    - can_edit
    - banned
    - state
    - author {id, username, avatar, ONLINE}
  - new items
  - last_id
- in_campaign
- campaign_id

## check_new_item

- request: POST

### check_new_item: _input_

- last_id
- CATEGORY_ID

### check_new_item: _output_

- code
- message
- data:
  - new_items

## response code

![response code](rc.png)
