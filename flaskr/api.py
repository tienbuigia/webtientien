from flask import (
    Blueprint, jsonify, current_app, g, request, flash
)
import sqlite3
from flaskr.auth import login_required
from flaskr.db import get_db


bp = Blueprint('api', __name__, url_prefix='/api')


def toJson(field_list, data_list):
    column_list = []
    for i in field_list:
        column_list.append(i[0])

    jsondata_list = []
    for row in data_list:
        data_dict = {}
        for i in range(len(column_list)):
            data_dict[column_list[i]] = row[i]
        jsondata_list.append(data_dict)
    return jsondata_list


def isValidPost(id, check_author=True):
    # check if valid post id
    post = get_db().execute(
        'SELECT p.id, title, body, created, author_id, username'
        ' FROM post p JOIN user u ON p.author_id = u.id'
        ' WHERE p.id = ?',
        (id,)
    ).fetchone()

    if post is None:
        return f"post {id} don't exist"

    if check_author and post['author_id'] != g.user['id']:
        return "permission wrong"

    return None


@bp.route('/')
def index():
    # db = get_db()
    con = sqlite3.connect(current_app.config['DATABASE'])
    cur = con.cursor()
    data_list = cur.execute(
        'SELECT p.id, title, body, created, author_id, username'
        ' FROM post p JOIN user u ON p.author_id = u.id'
        ' ORDER BY created DESC'
    ).fetchall()
    field_list = cur.description
    cur.close()
    con.close()
    return jsonify({'post': toJson(field_list, data_list)})


@bp.route('/create', methods=['GET', 'POST'])
@login_required
def create():
    if request.method == 'POST':
        title = request.form['title']
        body = request.form['body']
        error = None

        if not title:
            error = 'Title is required.'

        if error is not None:
            return error
        else:
            db = get_db()
            db.execute(
                'INSERT INTO post (title, body, author_id)'
                ' VALUES (?, ?, ?)',
                (title, body, g.user['id'])
            )
            db.commit()
            return "Success!"


@bp.route('/<int:id>/update', methods=['GET', 'POST'])
@login_required
def update(id):
    if request.method == 'POST':
        title = request.form['title']
        body = request.form['body']
        error = None

        if not title:
            error = 'Title is required.'

        error = isValidPost(id)

        if error is not None:
            return error
        else:
            db = get_db()
            db.execute(
                'UPDATE post SET title = ?, body = ?'
                ' WHERE id = ?',
                (title, body, id)
            )
            db.commit()
            return "Success!"


@bp.route('/<int:id>/delete', methods=['POST'])
@login_required
def delete(id):
    error = isValidPost(id)
    if error is not None:
        return error
    db = get_db()
    db.execute('DELETE FROM post WHERE id = ?', (id,))
    db.commit()
    return "Success!"
